using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UIElements;

public class ButtonEventHandler : MonoBehaviour
{
    [SerializeField] private List<string> _nameButtons = new List<string>();
    [SerializeField] private List<string> _nameScenes = new List<string>();
    private UIDocument _uiDocument;

    private List<VisualElement> _startButtons = new List<VisualElement>();

    // Start is called before the first frame update
    void Awake()
    {
        _uiDocument = FindObjectOfType<UIDocument>();

        foreach (var _nameButton in _nameButtons)
        {
            _startButtons.Add(_uiDocument.rootVisualElement.Query<Button>(_nameButton));
        }
    }

    private void OnEnable()
    {
        for (int i = 0; i < _nameButtons.Count; i++)
        {
            _startButtons[i].RegisterCallback<ClickEvent, string>(OnStartButtonMouseDownEvent, _nameScenes[i]);
        }
    }

    private void OnStartButtonMouseDownEvent(ClickEvent evt, string sceneName)
    {
        SceneManager.LoadSceneAsync(sceneName);
    }

    private void OnDisable()
    {
        for (int i = 0; i < _nameButtons.Count; i++)
        {
            _startButtons[i].UnregisterCallback<ClickEvent, string>(OnStartButtonMouseDownEvent);
        }
    }
}
