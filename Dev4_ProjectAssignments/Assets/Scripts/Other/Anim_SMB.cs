﻿using UnityEngine;
using UnityEngine.Events;

namespace GDD
{
    public class Anim_SMB : StateMachineBehaviour
    {
        private int index = 0;

        private UnityAction _stateEnterAction;
        private UnityAction _stateExitAction;
        private UnityAction _stateUpdateAction;
        private UnityAction _stateMoveAction;
        private UnityAction _stateIKAction;

        public UnityAction StateEnterAction
        {
            get => _stateEnterAction;
            set => _stateEnterAction = value;
        }

        public UnityAction StateExitAction
        {
            get => _stateExitAction;
            set => _stateExitAction = value;
        }

        public UnityAction StateUpdateAction
        {
            set => _stateUpdateAction = value;
        }
        public UnityAction StateMoveAction
        {
            set => _stateMoveAction = value;
        }
        public UnityAction StateIKAction
        {
            set => _stateIKAction = value;
        }
            

        override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _stateEnterAction.Invoke();
        }

        override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            //_stateExitAction.Invoke();
        }

        override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

        }

        override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

        }

        override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {

        }
    }
}